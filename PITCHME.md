---

# Presentation outline

* About Version Control
* Git's basic concept
* Quick example (local)
* Example using GitLab
* Collaboration and User Config

---

## Why Version Control?

* Keep track of changes
* Easy revert changes made by mistake
* Collaboration on a project
* Experiment without destroying everything

---

**Local Version Control Systems**

<img src="https://git-scm.com/book/en/v2/images/local.png" style="height: 400px; background: white;" />

---

**Distributed Version Control**

<img src="https://git-scm.com/book/en/v2/images/distributed.png" style="height: 400px; background: white;" />

*This is how Git works!*

---

**Git's basic concept**

![The lifecycle of the status of your files](https://git-scm.com/book/en/v2/images/lifecycle.png)

[https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository]()

---

**A quick example**

We will...

* Create a new Repository
* Add the file
* Commit it
* Update the file, add/stage it and commit again

---

Create a new Repository

```
# Go into your working directory:
cd /c/Users/rolandf/Desktop/my-project
# Initialize Git Repository:
git init
```

---

Create and add a file to the staging area

```
git add meine-datei.txt
# Show status:
git status
```

---

Commit your changes

```
git commit -m "Some usefull comment"
```

---

For the rest... we will see it live!
