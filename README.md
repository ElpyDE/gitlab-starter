
[![GitPitch](https://gitpitch.com/assets/badge.svg)](https://gitpitch.com/ElpyDE/gitlab-starter/master?grs=gitlab&t=moon)

# GitLab Starter at the Sound Localization Journal Club

## Goals

* Introduction to Version Control with Git
* Easy to follow/understand starter for newcomers
* Use Git and GitLab to show how Git and GitLab work

